# "This file […] should not contain commands that produce output or assume the
# shell is attached to a tty."
# — zsh introduction § 3
#
# Variables for non-interactive shells are exported here.
#
# $path order, if changed here, would be altered by "path_helper -s" (executed
# by /etc/zprofile, after this file). So $path changes are done in .zshrc.

# JVM variables {{{1
#
# See zshbuiltins(1) for typeset options.
typeset -gxTU JAVA_OPTS java_opts " "
typeset -gxTU JAVA_TOOL_OPTIONS java_tool_options " "
#
if [[ -r "$HOME"/.zsh/jvm/java.zsh ]]; then
  . "$HOME"/.zsh/jvm/java.zsh
fi
#
# Those are BIME private $java_opts.
if [[ -r "$HOME"/.zsh/jvm/bime.zsh ]]; then
  . "$HOME"/.zsh/jvm/bime.zsh
fi

# Brew {{{1
#
typeset -gxTU HOMEBREW_CASK_OPTS homebrew_cask_opts " "
homebrew_cask_opts=(
  '--appdir=/Applications'
  '--fontdir=/Library/Fonts')
