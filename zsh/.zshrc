# "This file is sourced in interactive shells. It should contain commands to
# set up aliases, functions, options, key bindings, etc."
# — zsh introduction § 3
#
# Variables for interactive shells are exported here.

# $path {{{1
#
# If there’s duplicates in $path, typeset -U tells to keep only the left-most
# occurrence. See zsh guide § 2.5.11.
typeset -U path
#
# Update $path, $manpath, $infopath and set variables for brew.
# $(brew --prefix) requires it.
eval "$(/opt/homebrew/bin/brew shellenv)"
#
path=("$HOME"/.n/bin "$(brew --prefix)/opt/mysql@5.7/bin" $path)

# AUTO_PUSHD {{{1
#
setopt AUTO_PUSHD
export DIRSTACKSIZE=8
#
# Tip: http://stackoverflow.com/a/4740090

# Brew {{{1
#
# Hide brew’s beer mug emoji.
export HOMEBREW_NO_EMOJI=1
#
export HOMEBREW_NO_ANALYTICS=1
#
if [[ -r "$HOME"/.dotfiles/do_not_commit/brew.sh ]]; then
  . "$HOME"/.dotfiles/do_not_commit/brew.sh
fi

# Colorization {{{1
#
# Enable ls colorization.
export CLICOLOR=

# Completion {{{1
#
# "If an ambiguous completion produces at least [2] possibilities, menu
# selection is started."
# — zsh guide § 6.2.3
zstyle ':completion:*' menu select=2
#
# Enable completion.
# — zsh guide § 6.3
autoload -U compinit
# "-u" is here to hide warnings when zsh is used by a user different than the
# owner of brew (read zsh manual § 20.2.1).
compinit -u
#
# "if you don't ever want to try old-style completion"
# — zsh guide § 6.3
zstyle ':completion:*' use-compctl false
#
# "provides compatibility with bash's programmable completion system."
# — zshall(1)
autoload -U bashcompinit
bashcompinit
#
if [[ -r "$(brew --prefix)/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/completion.zsh.inc" ]]; then
  . "$(brew --prefix)/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/completion.zsh.inc"
fi
if [[ -r "$(brew --prefix)/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/path.zsh.inc" ]]; then
  . "$(brew --prefix)/Caskroom/google-cloud-sdk/latest/google-cloud-sdk/path.zsh.inc"
fi
#
# Tips
#
# Undo expansion
#   ^_
# Abort menu selection
#   ^G

# Help {{{1
#
# "to access the online help"
# — zsh brew formula
#
autoload -U run-help
HELPDIR=$(brew --prefix)/share/zsh/helpfiles

# History {{{1
#
# See zshall(1) for these options.
setopt HIST_IGNORE_DUPS
setopt HIST_IGNORE_SPACE

# Key-bindings {{{1
#
# Set vi key-bindings. Note that setting $EDITOR or $VISUAL to vim does the
# same.
bindkey -v
#
# Tip
#
#   zshzle(1) to learn more about vi key-bindings.

# Prompt {{{1
#
# Set pure prompt.
autoload -U promptinit
promptinit
prompt pure

# BIME variables {{{1
#
if [[ -r "$HOME"/.zsh/variables/bime ]]; then
  . "$HOME"/.zsh/variables/bime
fi

# chruby {{{1
#
if [[ -r "$(brew --prefix)/opt/chruby/share/chruby/chruby.sh" ]]; then
  . "$(brew --prefix)/opt/chruby/share/chruby/chruby.sh"
  . "$(brew --prefix)/opt/chruby/share/chruby/auto.sh"
fi

# abduco {{{1
#
export ABDUCO_CMD=nvim

## EDITOR {{{1
#
export VISUAL=kak
export EDITOR=kak

# zdi {{{1
#
export BIME_IP=host.docker.internal
export ZENDESK_CODE_DIR="$HOME"/Code/zendesk
export DOCKER_FOR_MAC_ENABLED=true
if [[ -r "$HOME/Code/zendesk/zdi/dockmaster/zdi.sh" ]]; then
  . "$HOME/Code/zendesk/zdi/dockmaster/zdi.sh"
fi

# gpg {{{1
#
# gopass and "git commit -S" needs it. Without it, they fail with:
#   error: gpg failed to sign the data
#   fatal: failed to write commit object
export GPG_TTY=$(tty)

# n {{{1
#
# The default, /usr/local, isn’t writeable.
export N_PREFIX="$HOME/.n"

# iTerm2 {{{1
#
test -e "${HOME}/.iterm2_shell_integration.zsh" && . "${HOME}/.iterm2_shell_integration.zsh"

# aws-exec {{{1
#
if [[ -x "$HOME"/Code/zendesk/dotfiles_n_scripts/shell_scripts/aws-exec.bash ]]; then
  alias aws-exec="$HOME"/Code/zendesk/dotfiles_n_scripts/shell_scripts/aws-exec.bash
fi

# Recrewteer {{{1
#
export HEAVENT="$HOME/Code/heavent"
