I use [stow](https://www.gnu.org/software/stow/) to manage my dotfiles. Example:

    $ cd ~/.dotfiles
    $ stow git
