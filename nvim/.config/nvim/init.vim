" Tip
"
" Take changes into effect.
"   :source %


" vim options {{{1
"
"
" Set the system clipboard as the default register.
" :help clipboard-unnamed
set clipboard=unnamed
"
"
" "makes it possible to use :argdo and :bufdo to change a collection of
" buffers with a single command"
" — Practical Vim
set hidden
"
"
set ignorecase
"
" Tip
"
" \C to match case.
"   /blah\C
"
"
set inccommand=split
"
"
" Display unprintable characters.
set list
"
" Tip
"
" Toggle it.
"   :set list!
"
"
set number
"
"
" Used for :find and gf.
" - remove /usr/include from the default as I don’t code in C.
" - don’t use "**" here because it is too slow.
set path+=.,,node_modules
"
"
" Default is filnxtToOF, add 'I' to hide the intro message.
set shortmess=fiIlnxtToOF
"
"
set suffixesadd=.ts,.tsx,.js,.jsx
"
"
set switchbuf=usetab
"
"
" Upward search to find .git/tags.
" :help file-searching
"
" fugitive.vim loads tags from .git/tags, but I don’t use it.
set tags+=.git/tags;~
"
"
set termguicolors
"
"
autocmd FileType gitcommit setlocal spell
autocmd FileType text setlocal textwidth=78
autocmd BufNewFile,BufRead ~/Code/zendesk/{explore*,standalone}/* source ~/.config/nvim/explore.vim
" Don’t copy gopass passwords to unsecure directories.
autocmd BufNewFile,BufRead /dev/shm/gopass.* setlocal noswapfile nobackup noundofile


" mappings {{{1
"
"
" I use the bepo keyboard layout.
"
tnoremap <Esc><Esc> <C-\><C-n>
"
" Free CTRL-q as it’s the same as CTRL-V. This way I can use it as the detach
" key of abduco.
nmap <C-q> <NOP>
cmap <C-q> <NOP>
"
" List the buffers and prefill the :buffer command so that I only have to type
" a number then CR or a part of a buffer name then Tab and CR.
nnoremap gb :buffers<CR>:buffer<Space>


" color scheme {{{1
"
set background=light
let g:gruvbox_italic=1
colorscheme gruvbox


" :term {{{1
"
let $VISUAL = 'nvr -cc split --remote-wait'
" Nicer statusline in terminal mode, see :help terminal-emulator-status.
autocmd TermOpen * setlocal statusline=%{b:term_title}
autocmd TermOpen * setlocal nonumber


" Language Server Protocol {{{1
"
let g:LanguageClient_rootMarkers = {
  \ 'ts': ['tsconfig.json'],
  \ }
"
let g:LanguageClient_serverCommands = {
  \ 'ts': ['javascript-typescript-stdio'],
  \ }
