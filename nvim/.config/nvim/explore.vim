autocmd FileType coffee setlocal shiftwidth=2 textwidth=140 tabstop=2
autocmd FileType eruby setlocal expandtab shiftwidth=4 textwidth=140 tabstop=4
autocmd FileType html setlocal expandtab shiftwidth=2 textwidth=140 tabstop=4
autocmd FileType json setlocal expandtab shiftwidth=2 textwidth=140 tabstop=2
autocmd FileType less setlocal expandtab shiftwidth=4 textwidth=140 tabstop=4
autocmd FileType ruby setlocal expandtab shiftwidth=2 textwidth=140 tabstop=2
autocmd FileType scala setlocal expandtab shiftwidth=4 textwidth=140 tabstop=4
autocmd FileType typescript,typescriptreact setlocal expandtab shiftwidth=4 textwidth=140 tabstop=4
