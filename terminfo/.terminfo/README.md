# tic files

They are needed to get italics and true colors in various places (iTerm2
terminal, nvim :term, tmux, …).

`stow terminfo` links the compiled files where they need to be.

## iTerm2

Select the user profile, then in the “Terminal” pane, set
“xterm-256color-italic” in “Report Terminal Type” (don’t worry if it’s not in
the list).

Restart the terminal and test it with:

    echo `tput sitm`italics`tput ritm`
