# To export a key

    $ gpg --list-keys --keyid-format short

It outputs key IDs this way: rsu2048/A151B702 (A151B702 is the ID).

    $ gpg --armor --output fodier_pub_A151B702.asc --export A151B702
    $ gpg --armor --output fodier_priv_A151B702.asc \
          --export-secret-keys A151B702

# To import a key

Once I transfer keys through AirDrop for example, I like to clean them:

    $ xattr -c fodier_*

And check their rights. The pub key should be -rw-r--r--, the priv key should
be -rm-------.

    $ gpg --import fodier_priv_A151B702.asc
    $ gpg --edit-key A151B702 trust quit
    # enter 5<RET>
    # enter o<RET> (as I’m in French) or y<RET> (for English)
    $ gpg --list-keys
    # should now display "ultime" or "ultimate"
