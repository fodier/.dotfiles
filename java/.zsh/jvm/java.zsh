export JAVA_HOME=$(/usr/libexec/java_home -v 1.8)
java_opts=(
  '-server'
  '-Xms512M'
  '-Xmx1G'
  $java_opts)

# "-Djava.awt.headless=true"
#   prevents CLI apps from opening empty windows.
# "-Dfile.encoding=UTF-8"
#   forces encoding (a webapp I develop at work deals badly with encoding).
java_tool_options=(
  '-Djava.awt.headless=true'
  '-Dfile.encoding=UTF-8'
  $java_tool_options)
